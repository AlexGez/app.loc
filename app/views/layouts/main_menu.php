<?php
    $menu = Router::getMenu('menu_acl');
    $currentPage = currentPage();
?>

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
    <div class="container-fluid">
        <a class="navbar-brand" href="/home"><?=MENU_BRAND?></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main_menu" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="main_menu">
            <ul class="nav navbar-nav ">
                <?php foreach ($menu as $key => $val):
                    $active = ''; ?>
                <?php if (is_array($val)): ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <?=$key?>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <?php foreach ($val as $k => $v):
                                   $active = ($v == $currentPage)? 'active':''; ?>
                            <?php if ($k == 'separator'): ?>
                                <li role="separator" class="divider"></li>
                            <?php else: ?>
                                <li><a class="<?=$active?>" href="<?=$v?>"><?=$k?></a></li>
                            <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                <?php else:
                    $active = ($val == $currentPage)? 'active':''; ?>
                    <li><a class="nav-link <?=$active?>" href="<?=$val?>"><?=$key?></a></li>
                <?php endif; ?>
                <?php endforeach; ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if (currentUser()): ?>
                <li><a href="#">Hello, <?=currentUser()->user_name ?></a></li>
                <?php endif; ?>
            </ul>

        </div>
    </div>
</nav>