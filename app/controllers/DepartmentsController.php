<?php
class DepartmentsController extends Controller{
    public function __construct($controller, $action)
    {
        parent::__construct($controller, $action);
        $this->view->setLayout('default');
        $this->load_model('Departments');
    }

    public function indexAction()
    {
        $departments = $this->DepartmentsModel->find();
        $this->view->departments = $departments;
        $this->view->render('departments/index');
    }

    public function addAction()
    {
        $department = new Departments();
        $validation = new Validate();
        if ($_POST) {
            $department->assign($_POST);
            $validation->check($_POST, Departments::$addValidation);
            if ($validation->passed()) {
                $department->deleted = 0;
                $department->save();
                Router::redirect('departments');
            }
        }
        $this->view->department = $department;
        $this->view->displayErrors = $validation->displayErrors();
        $this->view->postAction = '/' . 'departments' . '/' . 'add';
        $this->view->render('departments/add');
    }

    // for soft-delete make
    //$this->_softDelete = true;
    // in tne models/Departments.php , but for delete fully - 'false'
    public function deleteAction($id)
    {
        $department = $this->DepartmentsModel->findById($id);
        if ($department) {
            $department->delete();
        }
        Router::redirect('departments');
    }


}