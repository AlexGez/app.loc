<form class="form" action="<?= $this->postAction ?>" method="POST">
    <div class="bg-dange form-errors" style="--bs-bg-opacity: .1;"><?=$this->displayErrors ?></div>
    <?= inputBlock('text','E-mail:', 'email', $this->contact->email, ['class'=>'form-control'],['class'=>'form-group col-md-6']); ?>
    <?= inputBlock('text','Имя:', 'name', $this->contact->name, ['class'=>'form-control'],['class'=>'form-group col-md-6']); ?>
    <?= inputBlock('text','Адрес:', 'address', $this->contact->address, ['class'=>'form-control'],['class'=>'form-group col-md-8']); ?>
    <?= inputBlock('text','Телефон:', 'phone', $this->contact->phone, ['class'=>'form-control'],['class'=>'form-group col-md-4']); ?>
    <?= inputBlock('text','Комментарии:', 'comments', $this->contact->comments, ['class'=>'form-control'],['class'=>'form-group col-md-12']); ?>

    <div class="form-group col-md-5">
        <label for="password">▼ Выберите отдел:</label>
        <select name="department" id="department" class="form-control">
            <!--                <h1>➽</h1>-->
            <?php foreach ($this->departments as $department): ?>
                <option value="<?=$department->id?>">
                    ➫ <?=$department->title?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="col-md-12 text-right">
        <a href="/contacts" class="btn btn-default">Отмена</a>
        <?= submitTag('Ок', ['class'=>'btn btn-primary']); ?>
    </div>
</form>