<?php
class Departments extends Model{

    public function __construct()
    {
        $table = 'departments';
        parent::__construct($table);
        // for soft-delete make 'true' below, but for delete fully - 'false'
        $this->_softDelete = false;

    }


    public static $addValidation = [
        'title' => [
            'display' => 'Отдел',
            'required' => true,
            'unique' => 'departments'
        ]
    ];


    public static function getDepartmentTitle($id)
    {
        return self::findById($id);
    }


}