<?php $this->start('body'); ?>

        <div class="row justify-content-center well">
            <div class="col-auto">
                <h2 class="text-center">
                    Отделы
                    <a href="/departments/add/" class="btn btn-outline-success">
                        <i class="glyphicon glyphicon-plus"></i>
                    </a>
                </h2>


                <table class="table-striped table-condensed table-bordered table-hover">
                    <thead>
                    <th>Отдел</th>
                    <th>Функции</th>
                    </thead>
                    <tbody>
                    <?php foreach ($this->departments as $department): ?>
                        <tr>
                            <td>
                                <?php echo $department->title; ?>
                            </td>
                            <td>
                                <a href="/departments/delete/<?= $department->id ?>"
                                   class="btn btn-outline-danger btn-xs"
                                   onclick="if (!confirm('Точно хочешь удалить этот отдел ?')){return false;}"
                                >
                                    <i class="glyphicon glyphicon-trash"> Удалить</i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

<?php $this->end(); ?>