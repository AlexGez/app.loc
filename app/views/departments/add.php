<?php $this->setSitetitle('Добавление Отдела'); ?>
<?php $this->start('body'); ?>
    <div class="col-md-8 col-md-offset-2 well">
        <h2 class="text-center">Добавить Новый Отдел</h2>
        <hr>
        <form class="form" action="<?= $this->postAction ?>" method="POST">
            <div class="bg-dange form-errors" style="--bs-bg-opacity: .1;"><?=$this->displayErrors ?></div>
            <?= inputBlock('text','Название отдела:', 'title', $this->department->title, ['class'=>'form-control'],['class'=>'form-group col-md-4']); ?>
            <div class="col-md-12 text-right">
                <?= submitTag('Добавить', ['class'=>'btn btn-success']); ?>
                <a href="/departments" class="btn btn-danger">Отмена</a>
            </div>
        </form>

    </div>
<?php $this->end(); ?>