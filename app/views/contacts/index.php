<?php $this->start('body'); ?>
    <h2 class="text-center">Сотрудники</h2>
    <div class="row justify-content-center well">
        <div class="col-auto">
            <table class="table-striped table-condensed table-bordered table-hover">
                <thead>
                    <th>Имя</th>
                    <th>Отдел</th>
                    <th></th>
                </thead>
                <tbody>
                <?php
                // foreach of departments_id from model Contacts
                foreach ($this->contacts as $contact): ?>
                    <tr>
                        <td>
                            <a href="/contacts/details/<?= $contact->id ?>" class="btn btn-default btn-xs">
                                <?php echo $contact->displayName(); ?>
                            </a>
                        </td>
                        <td><?php
                            // foreach of departments from model Department
                            foreach ($this->departments as $department){
                                if ($contact->department == $department->id){
                                    echo $department->title;
                                }
                            }
                            ?>
                        </td>
                        <td>
                            <a href="/contacts/edit/<?= $contact->id ?>" class="btn btn-info btn-xs">
                                <i class="glyphicon glyphicon-pencil"> Изменить</i>
                            </a>
                            <a href="/contacts/delete/<?= $contact->id ?>"
                               class="btn btn-danger btn-xs"
                               onclick="if (!confirm('Этот работник достаточно накосячил и нужно его удалить ?')){return false;}">
                                <i class="glyphicon glyphicon-remove"> Удалить</i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php ?>
                </tbody>
            </table>
        </div>
    </div>
<?php $this->end(); ?>