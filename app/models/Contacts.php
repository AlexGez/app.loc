<?php
class Contacts extends Model {

    public $id,$user_id,$email,$name,$address,$phone,$comments,$department,$deleted = 0;

    public function __construct()
    {
        $table = 'contacts';
        parent::__construct($table);
        // for soft-delete make 'true' below, but for delete fully - 'false'
        $this->_softDelete = true;

    }

    public static $addValidation = [
        'name' => [
            'display' => 'Имя',
            'required' => true,
            'max' => 100
        ],
        'email' => [
            'display' => 'E-mail',
            'required' => true,
            'unique' => 'contacts'
        ]
    ];

    public static $editValidation = [
        'name' => [
            'display' => 'Имя',
            'required' => true,
            'max' => 100
        ],
        'email' => [
            'display' => 'E-mail',
            'required' => true
        ]
    ];

    public function findAllByUserId($user_id, $params=[])
    {
        $conditions = [
            'conditions' => 'user_id = ?',
            'bind' => [$user_id]
        ];
        $conditions = array_merge($conditions, $params);
        return $this->find($conditions);
    }

    public function displayName()
    {
        return $this->name;
    }

    public function displayDepartmentTitle($id)
    {
        $conditions = [
            'conditions' => 'id = ?',
            'bind' => [$id]
        ];
        $department = Departments::getDepartmentTitle($id);
        dnd($department);
    }


    public function findByIdAndUserId($contact_id, $user_id, $params=[])
    {
        $conditions = [
            'conditions' => 'id = ? AND user_id = ?',
            'bind' => [$contact_id, $user_id]
        ];
        $conditions = array_merge($conditions,$params);
        return $this->findFirst($conditions);
    }

}