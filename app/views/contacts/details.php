<?php $this->setSiteTitle($this->contact->displayName()); ?>
<?php $this->start('body'); ?>
    <div class="col-md-8 col-md-offset-2 well">
        <a href="contacts" class="btn btn-xs btn-default">Назад</a>
        <h2 class="text-center"><?= $this->contact->displayName(); ?></h2>
        <div class="col-md-6">
            <p> <strong>E-mail:</strong> <?=$this->contact->email; ?></p>
            <p> <strong>Адрес:</strong> <?=$this->contact->address; ?></p>
            <p> <strong>Телефон:</strong> <?=$this->contact->phone; ?></p>
            <p> <strong>Комментарии:</strong> <?=$this->contact->comments; ?></p>
        </div>
    </div>
<?php $this->end(); ?>
