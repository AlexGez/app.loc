<?php
define('DEBUG' ,true);
define('DB_NAME', 'app.loc1'); // database name
define('DB_USER', 'root'); // database user
define('DB_PASSWORD', 'root'); // database password
define('DB_HOST', '127.0.0.1'); // database host ***use IP address to avoid DNS lookup

define('DEFAULT_CONTROLLER', 'HomeController'); //default controller if there isn't one defined in the url
define('DEFAULT_LAYOUT', 'default'); // if no layout is set in the controller use this layout.
define('PROOT', '/app.loc/');  // set this to '/' for a live server
define('SITE_TITLE', 'app.loc MVC framework'); // this will be used if no site-title is set
define('MENU_BRAND', 'APP.LOC'); // this is the Brand text in menu

define('CURRENT_USER_SESSION_NAME', 'kxXeusqldkiTKjehsLQZJFKJ'); // session name for logged in user
define('REMEMBER_ME_COOKIE_NAME', 'JAJEI6382LSJVlkdjfh3801jvD'); // cookie name for logged in user remember me
define('REMEMBER_ME_COOKIE_EXPIRY', 2592000); // time in seconds for remember me cookie to live (30days)

define('ACCESS_RESTRICTED', 'RestrictedController'); //controller name for the restricted redirect