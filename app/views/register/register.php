<?php $this->start('head'); ?>
<?php $this->end(); ?>
<?php $this->start('body'); ?>
<div class="col-md-6 col-md-offset-3 well">
    <h3 class="text-center">Регистрация Пользователя</h3>
    <hr>
    <form action="" class="form" method="POST">
        <div class="bg-dange form-errors" style="--bs-bg-opacity: .1;"><?=$this->displayErrors ?></div>
        <div class="form-group">
            <label for="user_name">Имя:</label>
            <input type="text" id="user_name" name="user_name" class="form-control" value="<?=$this->post['user_name']?>">
        </div>
        <div class="form-group">
            <label for="email">E-mail:</label>
            <input type="email" id="email" name="email" class="form-control" value="<?=$this->post['email']?>">
        </div>
        <div class="form-group">
            <label for="password">Придумайте пароль:</label>
            <input type="password" id="password" name="password" class="form-control" value="<?=$this->post['password']?>">
        </div>
        <div class="form-group">
            <label for="confirm">Подтвердите пароль:</label>
            <input type="password" id="confirm" name="confirm" class="form-control" value="<?=$this->post['confirm']?>">
        </div>
        <div class="pull-right">
            <input type="submit" class="btn btn-success btn-group-lg" value="Зарегистрироваться">
        </div>
    </form>
</div>
<?php $this->end(); ?>

