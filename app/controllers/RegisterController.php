<?php

class RegisterController extends Controller{


    public function __construct($controller, $action)
    {
        parent::__construct($controller, $action);
        $this->load_model('Users');
        $this->view->setLayout('default');
    }

    public function loginAction()
    {
        $validation = new Validate();
        if($_POST) {
            // form validation
            $validation->check($_POST, [
                'user_name' => [
                    'display' => "Имя",
                    'required' => true
                ],
                'password' => [
                    'display' => 'Пароль',
                    'required' => true,
                    'min' => 4
                ]
            ]);

            if ($validation->passed()) {
                $user = $this->UsersModel->findByUsername($_POST['user_name']);
                if ($user && password_verify(Input::get('password'), $user->password)) {
                    $remember = (isset($_POST['remember_me']) && Input::get('remember_me')) ? true : false;
                    $user->login($remember);
                    Router::redirect('');
                } else {
                    $validation->addError("Неправильный логин или пароль при авторизации");
                }
            }
        }
        $this->view->displayErrors = $validation->displayErrors();
        $this->view->render('register/login');
    }

    public function logoutAction()
    {
        if (currentUser()) {
            currentUser()->logout();
        }
        Router::redirect('register/login');
    }

    public function registerAction()
    {
        $validation = new Validate();
        $posted_values = ['user_name' => '', 'email' => '', 'password' => '', 'confirm' => ''];
        if ($_POST) {
            $posted_values = postedValues($_POST);
            $validation->check($_POST, [
                'user_name' => [
                    'display' => 'Имя пользователя',
                    'required' => true,
                    'unique' => 'users',
                    'min' => 4,
                    'max' => 100
                ],
                'email' => [
                    'display' => 'E-mail',
                    'required' => true,
                    'unique' => 'users',
                    'max' => 100,
                    'valid_email' => true
                ],
                'password' => [
                    'display' => 'Пароль',
                    'required' => true,
                    'min' => 4
                ],
                'confirm' => [
                    'display' => 'Подтверждение пароля',
                    'required' => true,
                    'matches' => 'password'
                ]
            ]);

            if ($validation->passed()) {
                $newUser = new Users();
                $newUser->registerNewUser($_POST);
                Router::redirect('register/login');
            }
        }

        $this->view->post = $posted_values;
        $this->view->displayErrors = $validation->displayErrors();
        $this->view->render('register/register');
    }



}