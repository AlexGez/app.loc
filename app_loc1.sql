-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 11 2022 г., 10:28
-- Версия сервера: 5.7.29
-- Версия PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `app.loc1`
--

-- --------------------------------------------------------

--
-- Структура таблицы `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `comments` text,
  `department` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `contacts`
--

INSERT INTO `contacts` (`id`, `user_id`, `email`, `name`, `address`, `phone`, `comments`, `department`, `deleted`) VALUES
(1, 1, 'alex@gmail.com', 'Alex', 'some one address str., 202', '0978713833', 'few first comments', 1, 0),
(2, 1, 'alex22@gmail.com', 'Alex222', 'some address str.,2', '123456789', 'few second comments', 4, 1),
(4, 1, 'alex5@gmail.com', 'Alex5', 'adv\\sdv', '3434564564', 'xbxcb', 5, 1),
(5, 1, 'alex7@gmail.com', 'Alex', 'alex street,7', '134234234', 'zvzdvzdfzdfbdzv\\sd', 1, 0),
(6, 1, 'alex8@gmail.com', 'Alex8', 'address,8', '876878876', 'comments', 4, 0),
(7, 1, 'alex9@gmail.com', 'Alex', 'address,9', '23423452', 'zvsdfsdvsdv', 4, 0),
(8, 1, 'alex10@gmail.com', 'Alex10', 'asdgasdgasd', '124123123123', 'commentsasdasdasdasd', 5, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `departments`
--

INSERT INTO `departments` (`id`, `title`, `deleted`) VALUES
(1, 'отдел 1', 0),
(4, 'Отдел 2', 0),
(5, 'Отдел 3', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `comments` text,
  `department` int(11) DEFAULT NULL,
  `acl` text,
  `deleted` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `user_id`, `user_name`, `email`, `password`, `address`, `phone`, `comments`, `department`, `acl`, `deleted`) VALUES
(1, 1, 'Alex', 'alex@gmail.com', '$2y$10$zVZ.uzVl3950O9F8fy.sQeit/FUbih84WAZLe1r1PxYf.mFoZYeV2', 'address,1', '0978713833', 'few comments-1', 1, NULL, 0),
(3, NULL, 'Alex2', 'alex2@gmail.com', '$2y$10$rby5RQhFFL94TtUMqlPLHeLk1an4K02DpIxDAbSTsxzRmWe0/hD8.', NULL, NULL, NULL, NULL, NULL, NULL),
(4, NULL, 'Alex3', 'alex3@gmail.com', '$2y$10$S5u/MPyb8ZxpAMnlYplYo.xISyR.j/iVWL15.Ea1ZsdB.f7LoUwG6', NULL, NULL, NULL, NULL, NULL, 0),
(5, NULL, 'Alex6', 'alex6@gmail.com', '$2y$10$JRfOF08xmEX4rT6ckM/6kuomBwydQ1u0iPukA96BdOMZKYA4VoMN6', NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `user_sessions`
--

CREATE TABLE `user_sessions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `session` varchar(255) NOT NULL,
  `user_agent` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_sessions`
--
ALTER TABLE `user_sessions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `user_sessions`
--
ALTER TABLE `user_sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
