<?php $this->start('head'); ?>
<?php $this->end(); ?>
<?php $this->start('body'); ?>
<div class="col-md-6 col-md-offset-3 well">
    <form class="form" action="/register/login" method="POST">
        <div class="bg-danger" style="--bs-bg-opacity: .1;"><?=$this->displayErrors ?></div>
        <h3 class="text-center">Авторизация</h3>
        <div class="form-group">
            <label for="user_name">Имя</label>
            <input type="text" name="user_name" id="user_name" class="form-control">
        </div>
        <div class="form-group">
            <label for="user_name">Пароль</label>
            <input type="password" name="password" id="password" class="form-control">
        </div>
        <div class="form-group">
            <label for="remember_me">Запомнить Меня
                <input type="checkbox" id="remember_me" name="remember_me" value="on">
            </label>
        </div>
        <div class="form-group">
            <input type="submit" value="Войти" class="btn btn-large btn-primary">
        </div>
        <div class="text-right">
            <a href="/register/register" class="text-primary">Зарегистрироваться</a>
        </div>
    </form>
</div>
<?php $this->end(); ?>

